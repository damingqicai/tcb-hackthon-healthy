import { View } from '@tarojs/components'
import Taro, { Config, useEffect, useState } from '@tarojs/taro'
import { AtButton, AtTextarea } from 'taro-ui'
import './index.less'

export function Anonymous() {
  const [value, setValue] = useState('')
  const [loading, setLoading] = useState(false)

  const report = async () => {
    setLoading(true)

    Taro.showLoading({
      title: '报告中',
    })

    await Taro.cloud.callFunction({
      name: 'faas',
      data: {
        method: 'anonymous_report',
        data: value
      }
    })

    Taro.hideLoading()
    Taro.showToast({
      title: '成功',
      icon: 'success',
      duration: 2000
    })

    setValue('')
    setLoading(false)
  }

  return (
    <View className="container">
      <View className="text">
        <AtTextarea
          value={value}
          onChange={(event) => setValue((event.target as any).value)}
          maxLength={200}
          height={Taro.pxTransform(379)}
          placeholder="请输入你想上报的信息，如姓名、健康状态、返校交通、历史行程等"
        />
      </View>

      <View className="btn">
        <AtButton onClick={report} disabled={loading} type="primary">提交</AtButton>
      </View>
    </View>
  )
}

Anonymous.config = {
  navigationBarTitleText: '信息上报',
} as Config
